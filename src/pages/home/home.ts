import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { DatePicker } from '@ionic-native/date-picker';

import { HttpService } from '../../libs/http.service';
import { MessageService } from '../../libs/message.service';
import { DateService } from '../../libs/date.service';

import { DetailsPage } from '../../pages/details/details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loadingMessage: String = '';
  loading: boolean = false;
  news: any = [];
  currentDay: String = '';
  year: number = 0;
  searchDay: String = '';
  teams: any = {};
  pendingTeams: any = [];

  constructor(public navCtrl: NavController, private translate: TranslateService, private app: App, private httpService: HttpService, private messages: MessageService, private date: DateService, private datePicker: DatePicker) {
    this.currentDay = '';
    this.searchDay = '';
    this.translate.get('LOADING').subscribe(
      value => {
        this.loadingMessage = value;
      }
    );
  }

  ionViewDidEnter(){
    this.init();
  }

  private init() {
    this.getTeams();
  }

  public changeDate(): void {
    this.datePicker.show({
      date: this.date.getRawDate(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.currentDay = this.date.trasformDateRaw(date);
        this.getDashBoard(this.date.trasformDate(date));
      },
      err => console.log(err)
      );
  }

  public getDashBoard(day: String): void {
    this.searchDay = day;
    this.loading = true;
    this.httpService.get({
      url: day + '/scoreboard.json',
      app: this.app,
      success: this.callBackDashBoard,
      context: this
    });
  }

  private callBackDashBoard(response): void {
    this.messages.closeMessage();
    this.news = [];
    let board = response.games;
    let boardLength = response.games.length;
    for (let i = 0; i < boardLength; i++) {
      let row = {
        start: '',
        finished: false,
        vScore: 0,
        gameId: 0,
        hScore: 0,
        vTeam: '',
        vClass: '',
        hClass: '',
        hTeam: '',
        country: '',
        city: '',
        arenaName: '',
        attendance: 0
      };

      row.start = this.date.getHour(board[i].startTimeUTC);
      row.gameId = board[i].gameId;
      row.attendance = board[i].attendance;
      row.vClass = '';
      row.hClass = '';
      if (board[i].endTimeUTC !== undefined && board[i].endTimeUTC !== null) {
        row.finished = true;
        row.vScore = Number(board[i].vTeam.score);
        row.hScore = Number(board[i].hTeam.score);
        if (row.vScore > row.hScore) {
          row.vClass = 'winner';
        } else {
          if (row.vScore < row.hScore) {
            row.hClass = 'winner';
          } else {
            row.vClass = 'draw';
            row.hClass = 'draw';
          }
        }

      } else {
        row.finished = false;
        row.vScore = 0;
        row.hScore = 0;
      }

      if (this.teams[board[i].vTeam.teamId] !== undefined && this.teams[board[i].vTeam.teamId] !== null) {
        row.vTeam = this.teams[board[i].vTeam.teamId];
      } else {
        row.vTeam = '';
      }

      if (this.teams[board[i].hTeam.teamId] !== undefined && this.teams[board[i].hTeam.teamId] !== null) {
        row.hTeam = this.teams[board[i].hTeam.teamId];
      } else {
        row.hTeam = '';
      }

      row.country = board[i].arena.country;
      row.city = board[i].arena.city;
      row.arenaName = board[i].arena.name;
      this.news.push(row);
    }
    this.loading = false;
  }

  public getTeams(): void {
    this.loading = true;

    this.year = this.date.getYearDate() - 1;
    this.messages.showMessage({
      content: this.loadingMessage
    });

      this.httpService.get({
        url: this.year + '/teams.json',
        success:(response) =>{
          let standardTeams = response.league.standard;
          let teamsLength = standardTeams.length;
          let currentTeams = [];
          for (let i = 0; i < teamsLength; i++) {
            currentTeams[standardTeams[i].teamId] = standardTeams[i].fullName;
          }
          this.teams = currentTeams;
          this.currentDay = this.date.getCurrentDateRaw();
          this.getDashBoard(this.date.getCurrentDate());
        },
        app: this.app,
        context:this
      });
  }

  public openDetails(details: any): void {
    this.navCtrl.push(DetailsPage, {
      gameId: details.gameId,
      gameDate: this.searchDay,
      hTeam: details.hTeam,
      vTeam: details.vTeam,
      attendance: details.attendance
    });
  }

}
