webpackJsonp([0],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_plus__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SessionService = (function () {
    function SessionService(storage, facebook, googlePlus, messages) {
        this.storage = storage;
        this.facebook = facebook;
        this.googlePlus = googlePlus;
        this.messages = messages;
        this.ignoreSession = false;
        this.destiny = {};
    }
    //verificamos si hay token para que no entremosen esta pantalla
    SessionService.prototype.getSessionStatus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('mode_facebook').then(function (data) {
                var _this = this;
                if (data !== null && data !== undefined && data !== false) {
                    //una sesión usando facebook, vemos si hubo sesión
                    this.facebook.getLoginStatus()
                        .then(function (rta) {
                        if (rta.status === 'connected') {
                            resolve('facebook'); // hay sesion en la app y en facebook
                        }
                        else {
                            _this.storage.remove('mode_facebook');
                            _this.storage.remove('mode_google_plus');
                            resolve(false); //no hay sesion facebook, ya no hay variables de sesion de la app
                        }
                    }, function (error) {
                        console.error(error); //cuando se cancela
                        resolve(false);
                    })
                        .catch(function (error) {
                        console.error(error); //cuando se cancela
                        resolve(false);
                    });
                }
                else {
                    //verificamos google plus
                    this.storage.get('mode_google_plus').then(function (data) {
                        console.log("googleplus");
                        console.log(data);
                        if (data !== null && data !== undefined && data !== false) {
                            this.googlePlus.trySilentLogin({ 'scopes': 'profile email' })
                                .then(function (result) {
                                resolve('google_plus'); // hay sesion en la app y en google_plus
                            }, function (error) {
                                console.error(error); //cuando se cancela
                                this.storage.remove('mode_facebook');
                                this.storage.remove('mode_google_plus');
                                resolve(false);
                            }.bind(this))
                                .catch(function (error) {
                                console.error(error); //cuando se cancela
                                resolve(false);
                            });
                        }
                        else {
                            resolve(false);
                        }
                    }.bind(this));
                }
            }.bind(_this));
        });
    };
    SessionService.prototype.setDestinySession = function (destiny, params) {
        this.destiny.target = destiny;
        this.destiny.params = params;
    };
    SessionService.prototype.cleanDestinySession = function () {
        this.destiny = {};
    };
    SessionService.prototype.getDestinySession = function () {
        return this.destiny;
    };
    SessionService.prototype.loginByFacebook = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.facebook.login(['public_profile', 'email', 'user_friends'])
                .then(function (rta) {
                console.log(rta);
                if (rta.status == 'connected') {
                    _this.initSession({
                        'mode_facebook': true,
                        'mode_google_plus': false
                    });
                    resolve('normal');
                }
                ;
            })
                .catch(function (error) {
                console.error(error); //cuando se cancela
                resolve(false);
            });
        });
    };
    SessionService.prototype.loginByGooglePlus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.googlePlus.logout()
                .then(function (success) {
                _this.googlePlus.login({ 'scopes': 'profile email', 'webClientId': '' })
                    .then(function (result) {
                    _this.initSession({
                        'mode_facebook': false,
                        'mode_google_plus': true
                    });
                    resolve(result);
                })
                    .catch(function (error) {
                    console.error(error); //cuando se cancela
                    resolve(false);
                });
            }, function (error) {
                _this.googlePlus.login({ 'scopes': 'profile email' })
                    .then(function (result) {
                    _this.initSession({
                        'mode_facebook': false,
                        'mode_google_plus': true
                    });
                    resolve(result);
                })
                    .catch(function (error) {
                    console.error(error); //cuando se cancela
                    resolve(false);
                });
            });
        });
    };
    SessionService.prototype.initSession = function (params) {
        this.storage.set('mode_facebook', params.mode_facebook);
        this.storage.set('mode_google_plus', params.mode_google_plus);
    };
    SessionService.prototype.closeSession = function () {
        this.messages.closeMessage();
        this.storage.remove('mode_facebook');
        this.storage.remove('mode_google_plus');
        this.facebookLogOut();
        this.googlePlusLogOut();
    };
    SessionService.prototype.facebookLogOut = function () {
        var _this = this;
        this.facebook.getLoginStatus()
            .then(function (rta) {
            console.log(rta.status);
            if (rta.status === 'connected')
                _this.facebook.logout();
        }, function (error) {
            console.error(error);
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    SessionService.prototype.googlePlusLogOut = function () {
        this.googlePlus.logout()
            .then(function (success) {
            console.log('googlePlus ', success);
        }, function (error) {
            console.error(error);
        })
            .catch(function (error) {
            console.error(error);
        });
        this.googlePlus.disconnect()
            .then(function (success) {
            console.log('googlePlus ', success);
        }, function (error) {
            console.error(error);
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    SessionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_plus__["a" /* GooglePlus */], __WEBPACK_IMPORTED_MODULE_2__message_service__["a" /* MessageService */]])
    ], SessionService);
    return SessionService;
}());

//# sourceMappingURL=session.service.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__libs_http_service__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__libs_message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__libs_date_service__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_details_details__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomePage = (function () {
    function HomePage(navCtrl, translate, app, httpService, messages, date, datePicker) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.translate = translate;
        this.app = app;
        this.httpService = httpService;
        this.messages = messages;
        this.date = date;
        this.datePicker = datePicker;
        this.loadingMessage = '';
        this.loading = false;
        this.news = [];
        this.currentDay = '';
        this.year = 0;
        this.searchDay = '';
        this.teams = {};
        this.pendingTeams = [];
        this.currentDay = '';
        this.searchDay = '';
        this.translate.get('LOADING').subscribe(function (value) {
            _this.loadingMessage = value;
        });
    }
    HomePage.prototype.ionViewDidEnter = function () {
        this.init();
    };
    HomePage.prototype.init = function () {
        this.getTeams();
    };
    HomePage.prototype.changeDate = function () {
        var _this = this;
        this.datePicker.show({
            date: this.date.getRawDate(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            _this.currentDay = _this.date.trasformDateRaw(date);
            _this.getDashBoard(_this.date.trasformDate(date));
        }, function (err) { return console.log(err); });
    };
    HomePage.prototype.getDashBoard = function (day) {
        this.searchDay = day;
        this.loading = true;
        this.httpService.get({
            url: day + '/scoreboard.json',
            app: this.app,
            success: this.callBackDashBoard,
            context: this
        });
    };
    HomePage.prototype.callBackDashBoard = function (response) {
        this.messages.closeMessage();
        this.news = [];
        var board = response.games;
        var boardLength = response.games.length;
        for (var i = 0; i < boardLength; i++) {
            var row = {
                start: '',
                finished: false,
                vScore: 0,
                gameId: 0,
                hScore: 0,
                vTeam: '',
                vClass: '',
                hClass: '',
                hTeam: '',
                country: '',
                city: '',
                arenaName: '',
                attendance: 0
            };
            row.start = this.date.getHour(board[i].startTimeUTC);
            row.gameId = board[i].gameId;
            row.attendance = board[i].attendance;
            row.vClass = '';
            row.hClass = '';
            if (board[i].endTimeUTC !== undefined && board[i].endTimeUTC !== null) {
                row.finished = true;
                row.vScore = Number(board[i].vTeam.score);
                row.hScore = Number(board[i].hTeam.score);
                if (row.vScore > row.hScore) {
                    row.vClass = 'winner';
                }
                else {
                    if (row.vScore < row.hScore) {
                        row.hClass = 'winner';
                    }
                    else {
                        row.vClass = 'draw';
                        row.hClass = 'draw';
                    }
                }
            }
            else {
                row.finished = false;
                row.vScore = 0;
                row.hScore = 0;
            }
            if (this.teams[board[i].vTeam.teamId] !== undefined && this.teams[board[i].vTeam.teamId] !== null) {
                row.vTeam = this.teams[board[i].vTeam.teamId];
            }
            else {
                row.vTeam = '';
            }
            if (this.teams[board[i].hTeam.teamId] !== undefined && this.teams[board[i].hTeam.teamId] !== null) {
                row.hTeam = this.teams[board[i].hTeam.teamId];
            }
            else {
                row.hTeam = '';
            }
            row.country = board[i].arena.country;
            row.city = board[i].arena.city;
            row.arenaName = board[i].arena.name;
            this.news.push(row);
        }
        this.loading = false;
    };
    HomePage.prototype.getTeams = function () {
        var _this = this;
        this.loading = true;
        this.year = this.date.getYearDate() - 1;
        this.messages.showMessage({
            content: this.loadingMessage
        });
        this.httpService.get({
            url: this.year + '/teams.json',
            success: function (response) {
                var standardTeams = response.league.standard;
                var teamsLength = standardTeams.length;
                var currentTeams = [];
                for (var i = 0; i < teamsLength; i++) {
                    currentTeams[standardTeams[i].teamId] = standardTeams[i].fullName;
                }
                _this.teams = currentTeams;
                _this.currentDay = _this.date.getCurrentDateRaw();
                _this.getDashBoard(_this.date.getCurrentDate());
            },
            app: this.app,
            context: this
        });
    };
    HomePage.prototype.openDetails = function (details) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_details_details__["a" /* DetailsPage */], {
            gameId: details.gameId,
            gameDate: this.searchDay,
            hTeam: details.hTeam,
            vTeam: details.vTeam,
            attendance: details.attendance
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"Y:\projects\nba_movil\src\pages\home\home.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-buttons left class="black-space"></ion-buttons>\n    <ion-title>\n      {{\'HOME\' | translate}}\n    </ion-title>\n    <button ion-button menuToggle end>\n      <ion-icon ios="md-menu" md="md-menu"></ion-icon>\n    </button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item-group>\n      <ion-item class="search">\n        <ion-row>\n          <ion-col offset-4 col-8 class="dashboard-calendar">\n            <span>{{currentDay}}</span>\n            <ion-icon ios="md-calendar" md="md-calendar" (click)="changeDate()"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item class="home-dashboard" *ngFor="let item of news" (click)="openDetails(item)">\n        <h2 class="time">{{item.city}}, {{item.country}}</h2>\n        <ion-row>\n          <ion-col col-4>\n            <ion-thumbnail>\n              <img src="assets/logos/teams/team1.png">\n            </ion-thumbnail>\n            <h2>{{item.vTeam}}</h2>\n          </ion-col>\n\n          <ion-col col-4 class="center">\n            <h1 *ngIf="item.finished" class="center {{item.vClass}}">{{item.vScore}}</h1>\n            <h1 *ngIf="item.finished" class="center line">-</h1>\n            <h1 *ngIf="item.finished" class="center {{item.hClass}}">{{item.hScore}}</h1>\n            <h1 *ngIf="!item.finished" class="center">\n              <div class="start">{{\'START_AT\' | translate}}</div>\n              {{item.start}}</h1>\n          </ion-col>\n\n          <ion-col col-4>\n            <ion-thumbnail>\n              <img src="assets/logos/teams/team2.png">\n            </ion-thumbnail>\n            <h2>{{item.hTeam}}</h2>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-item-group>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"Y:\projects\nba_movil\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_4__libs_http_service__["a" /* HttpService */], __WEBPACK_IMPORTED_MODULE_5__libs_message_service__["a" /* MessageService */], __WEBPACK_IMPORTED_MODULE_6__libs_date_service__["a" /* DateService */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__["a" /* DatePicker */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config_data__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
this.httpService.get({
    url:'http://localhost/fasttrack/api/public/login/token',
    success: function(res){
        console.log(res);
        this.myData = res;
    },
    context:this
});
*/
var HttpService = (function () {
    function HttpService(http, storage, messages) {
        this.http = http;
        this.storage = storage;
        this.messages = messages;
        this.context = this;
    }
    HttpService.prototype.setParams = function (params) {
        this.success = params.success;
        var isExternal = params.url.indexOf("http");
        if (isExternal === -1)
            this.url = __WEBPACK_IMPORTED_MODULE_2__config_config_data__["a" /* CONFIG */].ROUTES.DOMAIN_API + '/' + params.url;
        else
            this.url = params.url;
        this.error = function () { };
        this.finally = function () { };
        if (params.error !== undefined && typeof params.error == 'function')
            this.error = params.error;
        if (params.app !== undefined)
            this.app = params.app;
        if (params.finally !== undefined && typeof params.finally == 'function')
            this.finally = params.finally;
        if (params.context !== undefined)
            this.context = params.context;
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.search = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* URLSearchParams */]();
        if (params.bearToken !== undefined && params.bearToken !== null && params.bearToken !== '') {
            this.headers.append('Authorization', ' Bearer ' + params.bearToken);
        }
        this.options.headers = this.headers;
        if (params.urlParams !== undefined && params.urlParams instanceof Array) {
            var max_length = params.urlParams.length;
            for (var i = 0; i < max_length; i++) {
                var current = params.urlParams[i];
                if (typeof current == 'object') {
                    for (var key in current) {
                        if (key !== '')
                            this.url = this.url + '/' + key + '/' + current[key];
                        else
                            this.url = this.url + '/' + current[key];
                    }
                }
                else {
                    this.url = this.url + '/' + current;
                }
                this.options.search = this.search;
            }
        }
        if (params.urlAltParams !== undefined && typeof params.urlAltParams == 'object') {
            for (var key in params.urlAltParams) {
                this.search.set(key, params.urlAltParams[key]);
            }
            this.options.search = this.search;
        }
    };
    HttpService.prototype.get = function (params) {
        if (typeof params == 'object' && params.url !== undefined && typeof params.url == 'string' && params.url !== '' && params.success !== undefined && typeof params.success == 'function') {
            this.setParams(params);
            this.sendGet();
        }
    };
    HttpService.prototype.sendGet = function () {
        this.http.get(this.url, this.options)
            .map(function (response) { return response.json(); })
            .subscribe(this.successCallBack.bind({
            success: this.success.bind(this.context)
        }), this.errorCallBack.bind({
            error: this.error.bind(this.context),
            app: this.app,
            storage: this.storage,
            messages: this.messages
        }), this.finallyCallBack.bind({
            finally: this.finally
        }));
    };
    HttpService.prototype.successCallBack = function (response) {
        this.success(response);
    };
    HttpService.prototype.errorCallBack = function (error) {
        this.error(error);
    };
    HttpService.prototype.finallyCallBack = function () {
        this.finally();
    };
    HttpService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__message_service__["a" /* MessageService */]])
    ], HttpService);
    return HttpService;
}());

//# sourceMappingURL=http.service.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DateService = (function () {
    function DateService() {
    }
    DateService.prototype.getRawDate = function () {
        return new Date();
    };
    DateService.prototype.getCurrentDate = function () {
        var date = this.getRawDate();
        var completeDate = this.getProcessDate(date);
        return completeDate[0] + "" + completeDate[1] + "" + completeDate[2];
    };
    DateService.prototype.getCurrentDateRaw = function () {
        var date = new Date();
        var completeDate = this.getProcessDate(date);
        return completeDate[2] + '/' + completeDate[1] + '/' + completeDate[0];
    };
    DateService.prototype.trasformDate = function (date) {
        var completeDate = this.getProcessDate(date);
        return completeDate[0] + "" + completeDate[1] + "" + completeDate[2];
    };
    DateService.prototype.trasformDateRaw = function (date) {
        var completeDate = this.getProcessDate(date);
        return completeDate[2] + '/' + completeDate[1] + '/' + completeDate[0];
    };
    DateService.prototype.getProcessDate = function (date) {
        var day = date.getDate();
        if (day < 10)
            day = '0' + day.toString();
        var month = date.getMonth() + 1;
        if (month < 10)
            month = '0' + month.toString();
        return [date.getFullYear(), month, day];
    };
    DateService.prototype.getYearDate = function () {
        return new Date().getFullYear();
    };
    DateService.prototype.getFormatDate = function (date) {
        return this.getDate(date) + ' ' + this.getHour(date);
    };
    DateService.prototype.getDate = function (date) {
        var split = date.split('T');
        if (split.length === 2) {
            var dateSplit = split[0].split('-');
            if (dateSplit.length === 3)
                return dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];
        }
        return '';
    };
    DateService.prototype.getHour = function (date) {
        var split = date.split('T');
        if (split.length === 2) {
            var hourSplit = split[1].split(':');
            var hour = '';
            if (hourSplit.length === 3) {
                hour = hourSplit[0] + ':' + hourSplit[1];
            }
            return hour;
        }
        return '';
    };
    DateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DateService);
    return DateService;
}());

//# sourceMappingURL=date.service.js.map

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 124;

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 166;

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONFIG; });
var CONFIG = {
    ROUTES: {
        DOMAIN_API: 'http://data.nba.net/prod/v1',
        FOLDER_IMAGES: '/api/public/images'
    },
    LOGOS: {},
    GENERAL: {
        QUANTITY: 6,
        LANGUAGE: 'en'
    }
};
/*

sappitotech@gmail.com
Clave: urza1919
*/
//# sourceMappingURL=config.data.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_plus__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__libs_message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__libs_session_service__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = (function () {
    function LoginPage(navCtrl, facebook, sessionService, messages, googlePlus) {
        this.navCtrl = navCtrl;
        this.facebook = facebook;
        this.sessionService = sessionService;
        this.messages = messages;
        this.googlePlus = googlePlus;
        this.loadingMessage = '';
        this.errorLogin = '';
    }
    LoginPage.prototype.loginFacebook = function () {
        this.errorLogin = '';
        this.submitted = true;
        this.sessionService.loginByFacebook().then(function (result) {
            if (result !== false) {
                this.navegateLogin();
            }
            else {
                this.submitted = false;
            }
        }.bind(this), function (error) {
            this.errorLogin = error;
            this.submitted = false;
            //cerramos sesion en facebook y cerramos sesion en la app
            this.sessionService.closeSession();
        }.bind(this));
    };
    LoginPage.prototype.loginGooglePlus = function () {
        this.submitted = true;
        this.errorLogin = '';
        this.sessionService.loginByGooglePlus().then(function (result) {
            if (result !== false) {
                this.navegateLogin();
            }
            else {
                this.submitted = false;
                this.sessionService.googlePlusLogOut();
            }
        }.bind(this), function (error) {
            this.errorLogin = error;
            this.submitted = false;
            //cerramos sesion en facebook y cerramos sesion en la app
            this.sessionService.closeSession();
        }.bind(this));
    };
    LoginPage.prototype.navegateLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"Y:\projects\nba_movil\src\pages\login\login.html"*/'<ion-header class="not-color">\n  <ion-toolbar>\n    <ion-buttons left class="black-space"></ion-buttons>\n    <ion-title></ion-title>\n    <ion-buttons end class="black-space"></ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <content class="login">\n    <form>\n      <ion-row>\n        <ion-col col-12 class="center">\n          <span class="error center" [innerHTML]="errorLogin"></span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-12 class="center">\n          <img src="assets/logos/logo.png" alt="">\n          <h1>{{\'LOGIN\' | translate}}</h1>\n        </ion-col>\n      </ion-row>\n\n      <div class="buttons">\n        <button ion-button type="button" (click)="loginFacebook()" [disabled]="submitted" icon-left class="facebook">\n          <ion-icon ios="logo-facebook" md="logo-facebook"></ion-icon>\n          {{\'LOGIN_FACEBOOK\' | translate}}\n        </button>\n\n        <button ion-button type="button" (click)="loginGooglePlus()" [disabled]="submitted" icon-left class="google-plus">\n          <ion-icon ios="logo-googleplus" md="logo-googleplus"></ion-icon>\n          {{\'LOGIN_GOOGLEPLUS\' | translate}}\n        </button>\n      </div>\n\n    </form>\n  </content>\n</ion-content>\n'/*ion-inline-end:"Y:\projects\nba_movil\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_6__libs_session_service__["a" /* SessionService */], __WEBPACK_IMPORTED_MODULE_5__libs_message_service__["a" /* MessageService */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_plus__["a" /* GooglePlus */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__libs_http_service__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__libs_message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__libs_date_service__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DetailsPage = (function () {
    function DetailsPage(navCtrl, translate, app, httpService, date, messages, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.translate = translate;
        this.app = app;
        this.httpService = httpService;
        this.date = date;
        this.messages = messages;
        this.navParams = navParams;
        this.loadingMessage = '';
        this.hoursMessage = '';
        this.minutesMessage = '';
        this.gameId = '';
        this.gameDate = '';
        this.loading = false;
        this.broadcasters = {};
        this.details = {
            date: '00/00/0000',
            hour: '00:00:00',
            vTeam: '',
            totalVScore: 0,
            vScore: [],
            hTeam: '',
            totalHScore: 0,
            hScore: [],
            vClass: '',
            hClass: '',
            country: '',
            city: '',
            endTimeUTC: '0000-00-00T00:00:00',
            arenaName: '',
            attendance: 0,
            duration: '00:00:00',
            watch: [],
        };
        this.gameId = this.navParams.get('gameId');
        this.gameDate = this.navParams.get('gameDate');
        this.details.vTeam = this.navParams.get('vTeam');
        this.details.hTeam = this.navParams.get('hTeam');
        this.details.attendance = this.navParams.get('attendance');
        if (this.gameId === undefined || this.gameId === null || this.gameId === '' || this.gameDate === undefined || this.gameDate === null || this.gameDate === '' || this.details.vTeam === undefined || this.details.vTeam === null || this.details.vTeam === '' || this.details.hTeam === undefined || this.details.hTeam === null || this.details.hTeam === '')
            this.navCtrl.pop();
        this.translate.get('LOADING').subscribe(function (value) {
            _this.loadingMessage = value;
            _this.init();
        });
        this.translate.get('HOURS').subscribe(function (value) {
            _this.hoursMessage = value;
        });
        this.translate.get('MINUTES').subscribe(function (value) {
            _this.minutesMessage = value;
        });
    }
    DetailsPage.prototype.init = function () {
        this.getGameDetails();
    };
    DetailsPage.prototype.getGameDetails = function () {
        this.loading = true;
        this.httpService.get({
            url: this.gameDate + '/' + this.gameId + '_boxscore.json',
            app: this.app,
            success: this.callBackGameDetails,
            context: this
        });
    };
    DetailsPage.prototype.callBackGameDetails = function (response) {
        this.messages.closeMessage();
        var data = response.basicGameData;
        this.details.date = this.date.getDate(data.startTimeUTC);
        this.details.hour = this.date.getHour(data.startTimeUTC);
        this.details.country = data.arena.country;
        this.details.city = data.arena.city;
        this.details.arenaName = data.arena.name;
        if (data.endTimeUTC !== undefined && data.endTimeUTC !== null) {
            this.details.endTimeUTC = data.endTimeUTC;
            this.details.duration = data.gameDuration.hours + ' ' + this.hoursMessage + ', ' + data.gameDuration.minutes + ' ' + this.minutesMessage;
            var vScoreLength = data.vTeam.linescore.length;
            var vScore = data.vTeam.linescore;
            for (var i = 0; i < vScoreLength; i++) {
                this.details.vScore[i] = {
                    'type': 'period',
                    'points': vScore[i].score
                };
            }
            this.details.totalVScore = data.vTeam.score;
            this.details.vScore.push({
                'type': 'total',
                'points': data.vTeam.score
            });
            var hScoreLength = data.hTeam.linescore.length;
            var hScore = data.hTeam.linescore;
            for (var i = 0; i < hScoreLength; i++) {
                this.details.hScore[i] = {
                    'type': 'period',
                    'points': hScore[i].score
                };
            }
            this.details.totalHScore = data.hTeam.score;
            this.details.hScore.push({
                'type': 'total',
                'points': data.hTeam.score
            });
            if (this.details.totalVScore > this.details.totalHScore) {
                this.details.vClass = "winner";
                this.details.hClass = "loser";
            }
            else {
                if (this.details.totalVScore < this.details.totalHScore) {
                    this.details.vClass = "loser";
                    this.details.hClass = "winner";
                }
                else {
                    this.details.vClass = "draw";
                    this.details.hClass = "draw";
                }
            }
            this.details.watch = [];
        }
        else {
            this.details.endTimeUTC = '0000-00-00T00:00:00';
            this.details.duration = '00:00:00';
            this.details.totalVScore = 0;
            this.details.vScore = [];
            this.details.totalHScore = 0;
            this.details.hScore = [];
            this.getBroadCast(data.watch.broadcast.broadcasters.national);
            this.getBroadCast(data.watch.broadcast.broadcasters.canadian);
            this.getBroadCast(data.watch.broadcast.broadcasters.vTeam);
            this.getBroadCast(data.watch.broadcast.broadcasters.hTeam);
            this.getBroadCast(data.watch.broadcast.broadcasters.spanish_vTeam);
            this.getBroadCast(data.watch.broadcast.broadcasters.spanish_hTeam);
            this.getBroadCast(data.watch.broadcast.broadcasters.spanish_national);
            console.log(this.details.watch);
        }
        this.loading = false;
    };
    DetailsPage.prototype.getBroadCast = function (broadcast) {
        var broadcastLength = broadcast.length;
        for (var i = 0; i < broadcastLength; i++) {
            if (this.broadcasters[broadcast[i].longName] == undefined) {
                this.broadcasters[broadcast[i].longName] = true;
                this.details.watch.push(broadcast[i].shortName + ' (' + broadcast[i].longName + ')');
            }
        }
    };
    DetailsPage.prototype.backAction = function () {
        this.navCtrl.pop();
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"Y:\projects\nba_movil\src\pages\details\details.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-buttons left class="black-space">\n      <div class="center">\n        <ion-icon item-left name="md-arrow-back" (click)="backAction()"></ion-icon>\n      </div>\n    </ion-buttons>\n    <ion-title>\n      {{\'DETAILS\' | translate}}\n    </ion-title>\n    <button ion-button menuToggle end>\n      <ion-icon ios="md-menu" md="md-menu"></ion-icon>\n    </button>\n  </ion-toolbar>\n</ion-header>\n<ion-content class="details">\n  <ion-row class="header">\n    <ion-col col-5 class="visit">\n      <ion-thumbnail>\n        <img src="assets/logos/teams/team1.png">\n      </ion-thumbnail>\n      <div class="name">\n        <h2>{{details.vTeam}}</h2>\n        <h3 class="{{details.vClass}}" *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">{{details.totalVScore}}</h3>\n      </div>\n    </ion-col>\n    <ion-col col-2 class="score">\n      <h3>{{\'VS\' | translate}}</h3>\n    </ion-col>\n    <ion-col col-5 class="home">\n      <ion-thumbnail>\n        <img src="assets/logos/teams/team2.png">\n      </ion-thumbnail>\n      <div class="name">\n        <h2>{{details.hTeam}}</h2>\n        <h3 class="{{details.hClass}}" *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">{{details.totalHScore}}</h3>\n      </div>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-12 class="date">\n      {{details.date}}, {{details.hour}}\n    </ion-col>\n  </ion-row>\n\n  <ion-row *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">\n    <ion-col col-12 class="title-scores">\n      <ion-card>\n        <ion-card-title>\n          <ion-row>\n            <ion-col col-3 class="logo-score"></ion-col>\n            <ion-col col class="title" *ngFor="let item of details.hScore; let i = index">\n              <span *ngIf="item.type==\'period\'">{{(i+1)}}</span>\n              <b *ngIf="item.type==\'total\'">{{\'TOTAL\' | translate}}</b>\n            </ion-col>\n          </ion-row>\n        </ion-card-title>\n        <ion-card-content>\n          <ion-row *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">\n            <ion-col col-3 class="logo-score">\n                <img src="assets/logos/teams/team1.png">\n            </ion-col>\n            <ion-col col class="score" *ngFor="let item of details.vScore">\n                <span *ngIf="item.type==\'period\'">{{item.points}}</span>\n                <b *ngIf="item.type==\'total\'">{{item.points}}</b>\n            </ion-col>\n          </ion-row>\n          <ion-row *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">\n            <ion-col col-3 class="logo-score">\n              <img src="assets/logos/teams/team2.png">\n            </ion-col>\n            <ion-col col class="score" *ngFor="let item of details.hScore">\n                <span *ngIf="item.type==\'period\'">{{item.points}}</span>\n                <b *ngIf="item.type==\'total\'">{{item.points}}</b>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n\n    </ion-col>\n  </ion-row>\n\n\n  <ion-row *ngIf="details.endTimeUTC!=\'0000-00-00T00:00:00\'">\n    <ion-col col-6 class="stadium">\n      <ion-card>\n        <ion-card-title>{{\'ATTENDANCE\' | translate}}</ion-card-title>\n        <ion-card-content>\n          {{details.attendance}}\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n    <ion-col col-6 class="city">\n      <ion-card>\n        <ion-card-title>{{\'DURATION\' | translate}}</ion-card-title>\n        <ion-card-content>\n          {{details.duration}}\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n\n  <ion-row *ngIf="details.endTimeUTC==\'0000-00-00T00:00:00\'">\n    <ion-col col-6 class="stadium">\n      <ion-card>\n        <ion-card-title>{{\'STADIUM\' | translate}}</ion-card-title>\n        <ion-card-content>\n          {{details.arenaName}}\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n    <ion-col col-6 class="city">\n      <ion-card>\n        <ion-card-title>{{\'PLACE\' | translate}}</ion-card-title>\n        <ion-card-content>\n          {{details.city}}, {{details.country}}\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n\n  <ion-row *ngIf="details.endTimeUTC==\'0000-00-00T00:00:00\'">\n    <ion-col col-12 class="transmission">\n      <ion-item-group>\n       <ion-item-divider class="transmission">\n         {{\'TRANSMISSION\' | translate}}\n       </ion-item-divider>\n       <ion-item *ngFor="let item of details.watch" class="transmission-item">\n        <ion-badge item-end>{{item}}</ion-badge>\n       </ion-item>\n     </ion-item-group>\n    </ion-col>\n  </ion-row>\n\n</ion-content>\n'/*ion-inline-end:"Y:\projects\nba_movil\src\pages\details\details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_2__libs_http_service__["a" /* HttpService */], __WEBPACK_IMPORTED_MODULE_5__libs_date_service__["a" /* DateService */], __WEBPACK_IMPORTED_MODULE_3__libs_message_service__["a" /* MessageService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(237);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_globalization__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_date_picker__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_http_loader__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_component__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_login_login__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_home_home__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_details_details__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__libs_http_service__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__libs_message_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__libs_session_service__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__libs_date_service__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_11__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_details_details__["a" /* DetailsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_12__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_details_details__["a" /* DetailsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__["a" /* GooglePlus */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_globalization__["a" /* Globalization */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_18__libs_http_service__["a" /* HttpService */],
                __WEBPACK_IMPORTED_MODULE_19__libs_message_service__["a" /* MessageService */],
                __WEBPACK_IMPORTED_MODULE_20__libs_session_service__["a" /* SessionService */],
                __WEBPACK_IMPORTED_MODULE_21__libs_date_service__["a" /* DateService */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_globalization__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__libs_session_service__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_config_data__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, app, translateService, sessionService, globalization, menuCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.app = app;
        this.translateService = translateService;
        this.sessionService = sessionService;
        this.globalization = globalization;
        this.menuCtrl = menuCtrl;
        platform.ready().then(function () {
            _this.language();
        });
    }
    MyApp.prototype.language = function () {
        var _this = this;
        //Language
        if (this.platform.is('cordova')) {
            console.log("entrando");
            this.globalization.getPreferredLanguage().then(function (result) {
                var language = result.value.split('-')[0]; //evitamos cosas como -US
                _this.translateService.setDefaultLang(language);
                _this.runDevice();
                _this.sessionService.getSessionStatus().then(function (result) {
                    console.log("verificado", result);
                    var destiny;
                    if (result !== false) {
                        destiny = __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */];
                    }
                    else {
                        destiny = __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */];
                    }
                    //this.nav.setRoot(destiny);
                    this.rootPage = destiny;
                }.bind(_this));
            });
        }
        else {
            this.translateService.setDefaultLang(__WEBPACK_IMPORTED_MODULE_7__config_config_data__["a" /* CONFIG */].GENERAL.LANGUAGE);
            this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */];
            this.runDevice();
        }
    };
    MyApp.prototype.closeSession = function () {
        this.menuCtrl.close();
        this.sessionService.closeSession();
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.runDevice = function () {
        this.statusBar.styleBlackOpaque();
        //this.statusBar.styleDefault();
        this.splashScreen.hide();
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"Y:\projects\nba_movil\src\app\app.html"*/'<ion-menu class="menu" [content]="menu" [swipeEnabled]="false">\n  <ion-header>\n    <ion-toolbar color="secondary">\n      <ion-title>{{\'MENU\' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content>\n    <ion-list (click)="closeSession()">\n      <ion-item>{{\'SIGNOUT\' | translate}}</ion-item>\n    </ion-list>\n    <ion-list>\n      <ion-item menuClose detail-none>{{\'CLOSE\' | translate}}</ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n<ion-nav #menu [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"Y:\projects\nba_movil\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_6__libs_session_service__["a" /* SessionService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_globalization__["a" /* Globalization */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessageService = (function () {
    function MessageService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.isLoading = false;
    }
    MessageService.prototype.showMessage = function (params) {
        var config = {
            content: ''
        };
        console.log(params.spinner);
        if (params.spinner === undefined || params.spinner === null || params.spinner === true)
            config['spinner'] = 'crescent';
        else
            config['spinner'] = 'hide';
        if (params.content !== undefined)
            config['content'] = params.content;
        if (params.duration !== undefined)
            config['duration'] = params.duration;
        this.closeMessage();
        this.loading = this.loadingCtrl.create(config);
        if (params.onDisMiss !== undefined && typeof params.onDisMiss == 'function')
            this.loading.onDidDismiss = params.onDismiss;
        this.loading.present();
        this.isLoading = true;
    };
    MessageService.prototype.closeMessage = function () {
        if (this.isLoading) {
            this.loading.dismiss();
            this.isLoading = false;
        }
    };
    MessageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
    ], MessageService);
    return MessageService;
}());

//# sourceMappingURL=message.service.js.map

/***/ })

},[217]);
//# sourceMappingURL=main.js.map