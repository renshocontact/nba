import { Injectable } from '@angular/core';

@Injectable()
export class DateService {
  constructor() { }

  public getRawDate(): any {
    return new Date();
  }

  public getCurrentDate(): string {
    let date = this.getRawDate();
    let completeDate = this.getProcessDate(date);
    return completeDate[0] +""+ completeDate[1] +""+ completeDate[2];
  }

  public getCurrentDateRaw(): string {
    let date = new Date();
    let completeDate = this.getProcessDate(date);

    return completeDate[2] +'/'+ completeDate[1] +'/'+ completeDate[0];
  }

  public trasformDate(date:any): string {
    let completeDate = this.getProcessDate(date);

    return completeDate[0] +""+ completeDate[1] +""+ completeDate[2];
  }

  public trasformDateRaw(date:any): string {
    let completeDate = this.getProcessDate(date);

    return completeDate[2] +'/'+ completeDate[1] +'/'+ completeDate[0];
  }

  public getProcessDate(date:any): any {
    let day:any = date.getDate();
    if (day < 10)
      day = '0' + day.toString();

    let month:any = date.getMonth() + 1;
    if (month < 10)
      month = '0' + month.toString();

    return [date.getFullYear() , month , day];
  }

  public getYearDate(): number {
    return new Date().getFullYear();
  }

  public getFormatDate(date: string): string {
    return this.getDate(date) + ' ' + this.getHour(date);
  }

  public getDate(date: string): string {
    let split = date.split('T');
    if (split.length === 2) {
      let dateSplit = split[0].split('-');
      if (dateSplit.length === 3)
        return dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];
    }

    return '';
  }

  public getHour(date: string): string {
    let split = date.split('T');
    if (split.length === 2) {
      let hourSplit = split[1].split(':');
      let hour = '';
      if (hourSplit.length === 3) {
        hour = hourSplit[0] + ':' + hourSplit[1];
      }
      return hour;
    }

    return '';
  }
}
