import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

import { HomePage } from '../home/home';
import { MessageService } from '../../libs/message.service';
import { SessionService } from '../../libs/session.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  submitted: boolean;
  loadingMessage: string = '';
  errorLogin: String = '';

  constructor(public navCtrl: NavController, public facebook: Facebook, private sessionService: SessionService, public messages: MessageService, public googlePlus: GooglePlus) {

  }

  public loginFacebook(): void {
    this.errorLogin = '';
    this.submitted = true;
    this.sessionService.loginByFacebook().then(function(result) {
      if (result !== false) {
        this.navegateLogin();
      } else {
        this.submitted = false;
      }
    }.bind(this), function(error) {
      this.errorLogin = error;
      this.submitted = false;
      //cerramos sesion en facebook y cerramos sesion en la app
      this.sessionService.closeSession();
    }.bind(this));
  }

  public loginGooglePlus(): void {
    this.submitted = true;
    this.errorLogin = '';
    this.sessionService.loginByGooglePlus().then(function(result) {
      if (result !== false) {
        this.navegateLogin();
      } else {
        this.submitted = false;
        this.sessionService.googlePlusLogOut();
      }
    }.bind(this), function(error) {
      this.errorLogin = error;
      this.submitted = false;
      //cerramos sesion en facebook y cerramos sesion en la app
      this.sessionService.closeSession();
    }.bind(this));
  }

  public navegateLogin(): void {
    this.navCtrl.push(HomePage);
  }

}
