import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { CONFIG } from '../config/config.data';
import { MessageService } from './message.service';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { App } from 'ionic-angular';

/*
this.httpService.get({
    url:'http://localhost/fasttrack/api/public/login/token',
    success: function(res){
        console.log(res);
        this.myData = res;
    },
    context:this
});
*/

@Injectable()
export class HttpService {
  private url: string;
  private context: any = this;
  private success: any;
  private error: any;
  private finally: any;
  private options: any;
  private headers: any;
  private search: any;
  private app: App;

  constructor(private http: Http, private storage: Storage, public messages: MessageService) { }

  public setParams(params: any): void {
    this.success = params.success;
    let isExternal = params.url.indexOf("http");
    if (isExternal === -1)
      this.url = CONFIG.ROUTES.DOMAIN_API + '/' + params.url;
    else
      this.url = params.url;
    this.error = function() { };
    this.finally = function() { };
    if (params.error !== undefined && typeof params.error == 'function')
      this.error = params.error;
    if (params.app !== undefined)
      this.app = params.app;

    if (params.finally !== undefined && typeof params.finally == 'function')
      this.finally = params.finally;
    if (params.context !== undefined)
      this.context = params.context;
    this.options = new RequestOptions();
    this.headers = new Headers();
    this.search = new URLSearchParams();
    if (params.bearToken !== undefined && params.bearToken !== null && params.bearToken !== '') {
      this.headers.append('Authorization', ' Bearer ' + params.bearToken);
    }

    this.options.headers = this.headers;

    if (params.urlParams !== undefined && params.urlParams instanceof Array) {
      let max_length = params.urlParams.length;
      for (let i = 0; i < max_length; i++) {
        let current = params.urlParams[i];
        if (typeof current == 'object') {
          for (let key in current) {
            if (key !== '')
              this.url = this.url + '/' + key + '/' + current[key];
            else
              this.url = this.url + '/' + current[key];
          }
        } else {
          this.url = this.url + '/' + current;
        }
        this.options.search = this.search;
      }
    }

    if (params.urlAltParams !== undefined && typeof params.urlAltParams == 'object') {
      for (let key in params.urlAltParams) {
        this.search.set(key, params.urlAltParams[key]);
      }
      this.options.search = this.search;
    }
  }

  public get(params: any): any {
    if (typeof params == 'object' && params.url !== undefined && typeof params.url == 'string' && params.url !== ''  && params.success !== undefined && typeof params.success == 'function') {
      this.setParams(params);
      this.sendGet();
    }
  }

  public sendGet(): void {
    this.http.get(this.url, this.options)
    .map(response => response.json())
    .subscribe(this.successCallBack.bind(
      {
        success: this.success.bind(this.context)
      }), this.errorCallBack.bind(
      {
        error: this.error.bind(this.context),
        app: this.app,
        storage: this.storage,
        messages: this.messages
      }), this.finallyCallBack.bind(
      {
        finally: this.finally
      })
    );
  }

  private successCallBack(response: any): void {
    this.success(response);
  }

  private errorCallBack(error: any): void {
    this.error(error);
  }

  private finallyCallBack(): void {
    this.finally();
  }
}
