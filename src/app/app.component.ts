import { Component } from '@angular/core';
import { Platform, App, MenuController } from 'ionic-angular';
import { Globalization } from '@ionic-native/globalization';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../libs/session.service';
import { CONFIG } from '../config/config.data';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, private app: App, private translateService: TranslateService, private sessionService: SessionService, private globalization: Globalization, public menuCtrl: MenuController) {
    platform.ready().then(() => {
      this.language();
    });
  }

  private language():void{
    //Language
    if (this.platform.is('cordova')) {
    console.log("entrando");
      this.globalization.getPreferredLanguage().then(result => {
        let language = result.value.split('-')[0];//evitamos cosas como -US
        this.translateService.setDefaultLang(language);
        this.runDevice();
        this.sessionService.getSessionStatus().then(function(result) {
          console.log("verificado",result);
          let destiny:any;
          if(result!==false){
              destiny = HomePage;
          } else {
              destiny = LoginPage;
          }
          //this.nav.setRoot(destiny);
          this.rootPage = destiny;
        }.bind(this));
      });
    } else {
      this.translateService.setDefaultLang(CONFIG.GENERAL.LANGUAGE);
      this.rootPage = HomePage;
      this.runDevice();
    }
  }


  public closeSession(): void {
    this.menuCtrl.close();
    this.sessionService.closeSession();
    this.app.getRootNav().setRoot(LoginPage);
  }

  public runDevice(): void {
    this.statusBar.styleBlackOpaque();
    //this.statusBar.styleDefault();
    this.splashScreen.hide();
  }
}
