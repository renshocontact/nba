import { Component } from '@angular/core';
import { NavController, App, NavParams } from 'ionic-angular';
import { HttpService } from '../../libs/http.service';
import { MessageService } from '../../libs/message.service';
import { TranslateService } from '@ngx-translate/core';

import { DateService } from '../../libs/date.service';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {
  loadingMessage: String = '';
  hoursMessage: String = '';
  minutesMessage: String = '';
  gameId: string = '';
  gameDate: string = '';
  loading: boolean = false;
  broadcasters = {};
  details: any = {
    date: '00/00/0000',
    hour: '00:00:00',
    vTeam: '',
    totalVScore: 0,
    vScore: [],
    hTeam: '',
    totalHScore: 0,
    hScore: [],
    vClass: '',
    hClass: '',
    country: '',
    city: '',
    endTimeUTC: '0000-00-00T00:00:00',
    arenaName: '',
    attendance: 0,
    duration: '00:00:00',
    watch: [],
  };

  constructor(public navCtrl: NavController, private translate: TranslateService, private app: App, private httpService: HttpService, private date: DateService, private messages: MessageService, public navParams: NavParams) {
    this.gameId = this.navParams.get('gameId');
    this.gameDate = this.navParams.get('gameDate');
    this.details.vTeam = this.navParams.get('vTeam');
    this.details.hTeam = this.navParams.get('hTeam');
    this.details.attendance = this.navParams.get('attendance');
    if (this.gameId === undefined || this.gameId === null || this.gameId === '' || this.gameDate === undefined || this.gameDate === null || this.gameDate === '' || this.details.vTeam === undefined || this.details.vTeam === null || this.details.vTeam === '' || this.details.hTeam === undefined || this.details.hTeam === null || this.details.hTeam === '')
      this.navCtrl.pop();

    this.translate.get('LOADING').subscribe(
      value => {
        this.loadingMessage = value;
        this.init();
      }
    );

    this.translate.get('HOURS').subscribe(
      value => {
        this.hoursMessage = value;
      }
    );

    this.translate.get('MINUTES').subscribe(
      value => {
        this.minutesMessage = value;
      }
    );
  }

  private init() {
    this.getGameDetails();
  }

  public getGameDetails(): void {
    this.loading = true;
    this.httpService.get({
      url: this.gameDate + '/' + this.gameId + '_boxscore.json',
      app: this.app,
      success: this.callBackGameDetails,
      context: this
    });
  }

  private callBackGameDetails(response): void {
    this.messages.closeMessage();
    let data = response.basicGameData;
    this.details.date = this.date.getDate(data.startTimeUTC);
    this.details.hour = this.date.getHour(data.startTimeUTC);
    this.details.country = data.arena.country;
    this.details.city = data.arena.city;
    this.details.arenaName = data.arena.name;

    if (data.endTimeUTC !== undefined && data.endTimeUTC !== null) {
      this.details.endTimeUTC = data.endTimeUTC;
      this.details.duration = data.gameDuration.hours + ' ' + this.hoursMessage + ', ' + data.gameDuration.minutes + ' ' + this.minutesMessage;
      let vScoreLength = data.vTeam.linescore.length;
      let vScore = data.vTeam.linescore;

      for (let i = 0; i < vScoreLength; i++) {
        this.details.vScore[i] = {
          'type': 'period',
          'points': vScore[i].score
        };
      }
      this.details.totalVScore = data.vTeam.score;
      this.details.vScore.push({
        'type': 'total',
        'points': data.vTeam.score
      });

      let hScoreLength = data.hTeam.linescore.length;
      let hScore = data.hTeam.linescore;
      for (let i = 0; i < hScoreLength; i++) {
        this.details.hScore[i] = {
          'type': 'period',
          'points': hScore[i].score
        };
      }
      this.details.totalHScore = data.hTeam.score;
      this.details.hScore.push({
        'type': 'total',
        'points': data.hTeam.score
      });

      if (this.details.totalVScore > this.details.totalHScore) {
        this.details.vClass = "winner";
        this.details.hClass = "loser";
      } else {
        if (this.details.totalVScore < this.details.totalHScore) {
          this.details.vClass = "loser";
          this.details.hClass = "winner";
        } else {
          this.details.vClass = "draw";
          this.details.hClass = "draw";
        }
      }

      this.details.watch = [];
    } else {
      this.details.endTimeUTC = '0000-00-00T00:00:00';
      this.details.duration = '00:00:00';
      this.details.totalVScore = 0;
      this.details.vScore = [];
      this.details.totalHScore = 0;
      this.details.hScore = [];

      this.getBroadCast(data.watch.broadcast.broadcasters.national);
      this.getBroadCast(data.watch.broadcast.broadcasters.canadian);
      this.getBroadCast(data.watch.broadcast.broadcasters.vTeam);
      this.getBroadCast(data.watch.broadcast.broadcasters.hTeam);
      this.getBroadCast(data.watch.broadcast.broadcasters.spanish_vTeam);
      this.getBroadCast(data.watch.broadcast.broadcasters.spanish_hTeam);
      this.getBroadCast(data.watch.broadcast.broadcasters.spanish_national);

      console.log(this.details.watch);
    }
    this.loading = false;
  }

  private getBroadCast(broadcast:any):void{
    let broadcastLength = broadcast.length;
    for (let i = 0; i < broadcastLength; i++) {
      if (this.broadcasters[broadcast[i].longName] == undefined) {
        this.broadcasters[broadcast[i].longName] = true;
        this.details.watch.push(broadcast[i].shortName + ' (' + broadcast[i].longName + ')');
      }
    }
  }

  public backAction(): void {
    this.navCtrl.pop();
  }
}
