import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MessageService } from './message.service';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

@Injectable()
export class SessionService {
  ignoreSession: boolean = false;
  destiny: any = {};
  constructor(private storage: Storage, private facebook: Facebook, public googlePlus: GooglePlus, public messages: MessageService) { }
  //verificamos si hay token para que no entremosen esta pantalla
  public getSessionStatus(): any {
    return new Promise((resolve, reject) => {
      this.storage.get('mode_facebook').then(
        function(data: any) {
          if (data !== null && data !== undefined && data !== false) {
            //una sesión usando facebook, vemos si hubo sesión
            this.facebook.getLoginStatus()
              .then(rta => {
                if (rta.status === 'connected') {
                  resolve('facebook'); // hay sesion en la app y en facebook
                } else {
                  this.storage.remove('mode_facebook');
                  this.storage.remove('mode_google_plus');
                  resolve(false); //no hay sesion facebook, ya no hay variables de sesion de la app
                }
              }, function(error) {
                console.error(error);//cuando se cancela
                resolve(false);
              })
              .catch(error => {
                console.error(error);//cuando se cancela
                resolve(false);
              });
          } else {
            //verificamos google plus
            this.storage.get('mode_google_plus').then(
              function(data) {
                console.log("googleplus");
                console.log(data);
                if (data !== null && data !== undefined && data !== false) {
                  this.googlePlus.trySilentLogin({ 'scopes': 'profile email' })
                    .then(result => {
                      resolve('google_plus'); // hay sesion en la app y en google_plus
                    }, function(error) {
                      console.error(error);//cuando se cancela
                      this.storage.remove('mode_facebook');
                      this.storage.remove('mode_google_plus');
                      resolve(false);
                    }.bind(this))
                    .catch(error => {
                      console.error(error);//cuando se cancela
                      resolve(false);
                    });
                } else {
                  resolve(false);
                }
              }.bind(this)
            );
          }
        }.bind(this)
      );
    });
  }

  public setDestinySession(destiny: any, params: any): void {
    this.destiny.target = destiny;
    this.destiny.params = params;
  }

  public cleanDestinySession(): void {
    this.destiny = {};
  }

  public getDestinySession(): any {
    return this.destiny;
  }

  public loginByFacebook(): any {
    return new Promise((resolve, reject) => {
      this.facebook.login(['public_profile', 'email','user_friends'])
        .then(rta => {
          console.log(rta);
          if (rta.status == 'connected') {
            this.initSession({
              'mode_facebook': true,
              'mode_google_plus': false
            });
            resolve('normal');
          };
        })
        .catch(error => {
          console.error(error);//cuando se cancela
          resolve(false);
        });
    });
  }

  public loginByGooglePlus(): any {
    return new Promise((resolve, reject) => {
      this.googlePlus.logout()
        .then(success => {
          this.googlePlus.login({ 'scopes': 'profile email','webClientId':'' })
            .then(result => {
              this.initSession({
                'mode_facebook': false,
                'mode_google_plus': true
              });
              resolve(result);
            })
            .catch(error => {
              console.error(error);//cuando se cancela
              resolve(false);
            });
        }, (error) => {
          this.googlePlus.login({ 'scopes': 'profile email' })
            .then(result => {
              this.initSession({
                'mode_facebook': false,
                'mode_google_plus': true
              });
              resolve(result);
            })
            .catch(error => {
              console.error(error);//cuando se cancela
              resolve(false);
            });
        });
    });
  }

  public initSession(params: any): void {
    this.storage.set('mode_facebook', params.mode_facebook);
    this.storage.set('mode_google_plus', params.mode_google_plus);
  }

  public closeSession(): void {
    this.messages.closeMessage();
    this.storage.remove('mode_facebook');
    this.storage.remove('mode_google_plus');
    this.facebookLogOut();
    this.googlePlusLogOut();
  }

  public facebookLogOut(): void {
    this.facebook.getLoginStatus()
      .then(rta => {
        console.log(rta.status);
        if (rta.status === 'connected')
          this.facebook.logout();
      }, function(error) {
        console.error(error);
      })
      .catch(error => {
        console.error(error);
      });
  }

  public googlePlusLogOut(): void {
    this.googlePlus.logout()
    .then(success => {
      console.log('googlePlus ', success);
    }, function(error) {
      console.error(error);
    })
    .catch(error => {
      console.error(error);
    });

    this.googlePlus.disconnect()
    .then(success => {
      console.log('googlePlus ', success);
    }, function(error) {
      console.error(error);
    })
    .catch(error => {
      console.error(error);
    });
  }

}
